

@extends('layouts.masterE')

@section('content')

@if(count($errors))
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>

@endif


<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->

    </div>   <!-- koniec pierwszej kolumny rozm 1 -->

    <div class="col-md-8"> <!-- pierwszy wiersz, kolumna druga rozm 8 -->
      
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-8">
    <!-- Tu wpisz treśc strony -->

  <div class="col-sm-6 col-md-4 col-md-offset-4 ">
                    <h2>Sign in</h2>
                    <br>
                   {{--  <div class="account-wall"> --}}
                    {!! Form::open(array('route' =>'handleLogin', 'class' =>'form-signin')) !!}

                    <div class="form-group">
          					{!! Form::label('login', 'Login') !!}
          					{!! Form::text('login', null, ['class' =>'form-control', 'placeholder'=>'Login']) !!}	
          					</div>

          					<div class="form-group">
          					{!! Form::label('password', 'Password') !!}
          					{!! Form::password('password', ['class' =>'form-control', 'placeholder'=>'Password']) !!}	
          					</div>

          					{!! Form::token() !!}
          					{!! Form::submit('Sign in', array('class' =>'bbtn btn-lg btn-primary btn-block')) !!}
                      
                      {!! Form::close() !!}

                 {{--    </div> --}}
                    <div class="text-center">
                    <br>
                        Do you haven't account? <a href="/usersENG/createE" class="new-account">Register now!</a>
                    </div>
                </div>
           

                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>
<br><br>

</main>


	
@endsection