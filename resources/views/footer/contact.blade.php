@extends('layouts.master')

@section('content')


<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
     <br> <br> <br>
   <br> <br> <br>
  <div class="row">
  <div class="span3">
    <div class="well">
        <div>
            <ul class="nav nav-list">
                <li><label class="tree-toggle nav-header">Dzieci</label>
                    <ul class="nav nav-list tree">
                          <li><a href="/projects/projectCategory/ubranka">Ubranka</a></li>
                          <li><a href="/projects/projectCategory/zabawki">Zabawki</a></li>
                          <li><a href="/projects/projectCategory/akcesoria">Akcesoria</a></li>
                      <!--   <li><label class="tree-toggle nav-header">Next</label>
                            <ul class="nav nav-list tree">
                                <li><a href="#">Next</a></li>
                                <li><a href="#">Next</a></li>
                                <li><label class="tree-toggle nav-header">Next</label>
                                    <ul class="nav nav-list tree">
                                        <li><a href="#">Next</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Dom</label>
                    <ul class="nav nav-list tree">
                         <li><a href="/projects/projectCategory/meble">Meble</a></li>
                         <li><a href="/projects/projectCategory/tekstylia">Tekstylia</a></li>
                         <li><a href="/projects/projectCategory/ogród">Ogród</a></li>
                         <li><a href="/projects/projectCategory/dekoracje">Dekoracje</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Odzież damska</label>
                    <ul class="nav nav-list tree">
                     <li><a href="/projects/projectCategory/dbluzki">Bluzki</a></li>
                     <li><a href="/projects/projectCategory/dspodnie">Spodnie</a></li>
                     <li><a href="/projects/projectCategory/dsukienki">Sukienki</a></li>
                     <li><a href="/projects/projectCategory/dspódnice">Spódnice</a></li>
                     <li><a href="/projects/projectCategory/dwierzchnia">Odzież wierzchnia</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Odzież męska</label>
                    <ul class="nav nav-list tree">
                       <li><a href="/projects/projectCategory/mkoszulki">Koszulki</a></li>
                       <li><a href="/projects/projectCategory/mspodnie">Spodnie</a></li>
                       <li><a href="/projects/projectCategory/mkoszule">Koszule</a></li>
                       <li><a href="/projects/projectCategory/mmarynarki">Marynarki</a></li>
                      <li><a href="/projects/projectCategory/mwierzchnia">Odzież wierzchnia</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    </div>
</div>

<script type="text/javascript">
  $('.tree-toggle').click(function () {
 $(this).parent().children('ul.tree').toggle(200);
    });
  $(function(){
  $('.tree-toggle').parent().children('ul.tree').toggle(200);
  })
</script> 
<!-- koniec -->

    </div>   <!-- koniec pierwszej kolumny rozm 2 -->

    <div class="col-md-6"> <!-- pierwszy wiersz, kolumna druga rozm 7 -->
   
        <div class="container bg">

          <div class="row">
               
  
              <div class="row">
              <div class="col-md-3">
              </div>
              <div class="col-md-5">
                <h3>Formularz kontaktowy</h3>
                </div>

                </div>
<br>
		{!! Form::open(array('class'=>'form-horizontal')) !!}

<div class="row">
  <div class="col-md-3">
                 <!-- 
                 -->

  </div>

            <div class="form-group col-md-5">

            {!! Form::label('titleContact', 'Temat:', ['class' =>'col-form-label']) !!}

            {!!  Form::text('titleContact', null, ['class' =>'form-control', 'placeholder'=>'Temat', 'required']) !!}
            </div>
</div>
<div class="row">
  <div class="col-md-3">
                 <!-- 
                 -->
  </div>
  			<div class="form-group col-md-5">

            {!! Form::label('email', 'Email:', ['class' =>'col-form-label']) !!}

            {!!  Form::text('email', null, ['class' =>'form-control', 'placeholder'=>'E-mail nadawcy', 'required']) !!}
            </div>
</div>
<div class="row">
  <div class="col-md-3">
                 <!-- 
                 -->
  </div>
            <div class="form-group col-md-5">
            {!! Form::label('message', 'Wiadomość:') !!}
            {!! Form::textarea('message', null, ['class' =>'form-control', 'placeholder'=>'Wiadomość', 'required']) !!} 
            </div>
</div>

{{-- 
            {!! Form::label('subCategory', 'Kategoria') !!}
            {!! Form::select('subCategory', array(
                    'Dzieci' => array('ubranka' => 'Ubranka',
                                      'zabawki' => 'Zabawki',
                                      'akcesoria' => 'akcesoria'),

                    'Dom' => array( 'meble' => 'Meble',
                                    'tekstylia' => 'Tekstylia',
                                    'ogród' => 'Ogród',
                                    'dekoracje' => 'Dekoracje'),

                    'Odzież damska' => array('dbluzki' => 'Bluzki',
                                             'dspodnie' => 'Spodnie',
                                             'dsukienki' => 'Sukienki',
                                             'dspódnice' => 'Spódnice',
                                             'dwierzchnia' => 'Odzież wierzchnia'),

                    'Odzież męska' => array( 'mkoszulki' => 'Koszulki',
                                             'mspodnie' => 'Spodnie',
                                             'mkoszule' => 'Koszule',
                                             'mmarynarki' => 'Marynarki',
                                             'mwierzchnia' => 'Odzież wierzchnia'),
                )); 
            !!} --}}

         
<br>
            {!! Form::token() !!}
            {!! Form::submit('Wyślij wiadomość', array('class' =>'btn btn-default')) !!}
            
        {!! Form::close() !!}
                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


</main>

@endsection