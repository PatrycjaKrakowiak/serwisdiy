@extends('layouts.masterE')

@section('content')
<?php
require "../vendor/autoload.php";
?>
 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> <br> <br>
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li ><a href="/adminE"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/allProjectE"><i class="fa fa-book fa-fw"></i>Added projects</a></li>
                <li><a href="/allUsersE"><i class="fa fa-user fa-fw"></i>All users</a></li>
                <li class="active"><a href="/statsE"><i class="fa fa-chart fa-fw"></i>Stats</a></li>
             
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
          Stats

          
<br>
<?php
$votes  = Lava::DataTable();

$votes->addStringColumn('Food Poll')
      ->addNumberColumn('Votes')
      ->addRow(['Tacos',  rand(1000,5000)])
      ->addRow(['Salad',  rand(1000,5000)])
      ->addRow(['Pizza',  rand(1000,5000)])
      ->addRow(['Apples', rand(1000,5000)])
      ->addRow(['Fish',   rand(1000,5000)]);

$chart2=Lava::ColumnChart('Votes', $votes);

?>
<div id="stocks-chart">
<?php
echo Lava::render('ColumnChart', 'Votes', 'stocks-chart');
?>

</div>

		</div>


        </div>
    </div>
</div>


    </main>



@endsection


