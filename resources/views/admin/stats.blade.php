@extends('layouts.master')

@section('content')
<?php
require "../vendor/autoload.php";
?>
 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> <br> <br>
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li ><a href="/admin"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/allProject"><i class="fa fa-book fa-fw"></i>Dodane projekty</a></li>
                <li><a href="/allUsers"><i class="fa fa-user fa-fw"></i>Wszyscy użytkownicy</a></li>
                <li class="active"><a href="/stats"><i class="fa fa-chart fa-fw"></i>Statystyki</a></li>
             
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
       <h3>   Statystyki </h3>

          
<br><?php
$ubranka = DB::table('projects')
                     ->select(DB::raw('count(*) as ile'))
                     ->where('subCategory', '=', "ubranka")
                     ->groupBy('subCategory')
                     ->get();

foreach ($ubranka as $value) {
  $liczbaU= $value->ile;
}

$ubranka = DB::table('projects')
                     ->select(DB::raw('count(*) as ile'))
                     ->where('subCategory', '=', "zabawki")
                     ->groupBy('subCategory')
                     ->get();

foreach ($ubranka as $value) {
  $liczbaZ= $value->ile;
}
?>
<br>
<?php
$votes  = Lava::DataTable();


$votes->addStringColumn('Food Poll')
      ->addNumberColumn('Ile')
      ->addRow(['Ubranka',  $liczbaU])
      ->addRow(['Zabawki',  $liczbaZ]);

$chart2=Lava::ColumnChart('Ile', $votes);

?>
<div id="stocks-chart">
<?php
echo Lava::render('ColumnChart', 'Ile', 'stocks-chart');
?>

</div>

		</div>


        </div>
    </div>
</div>


    </main>



@endsection


