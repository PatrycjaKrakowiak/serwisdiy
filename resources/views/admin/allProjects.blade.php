@extends('layouts.master')

@section('content')
 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> <br> <br>
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/admin"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li class="active"><a href="/allProjects"><i class="fa fa-book fa-fw"></i>Dodane projekty</a></li>
                <li><a href="/allUsers"><i class="fa fa-user fa-fw"></i>Wszyscy użytkownicy</a></li>
                <li><a href="/stats"><i class="fa fa-line-chart fa-fw"></i>Statystyki</a></li>
             
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
             <div class="col-md-1">
            
              </div>
          
            <div class="col-md-10">
               <h3> Dodane projekty: </h3>


            <?php
             
            
                    foreach ($userProject as $value) 
                    {
                       
                            $foto = $value->fotoProject;
                            $idFoto = $value->idProject;
                            
                        ?>
                     <a href="/../projects/aboutProject/{{$idFoto}}">    
                        <img src="/upload/uploadPhoto/{{$foto}}" alt="projekt" width="150" height="120"> 
                     </a>
                        
                      <?php
                       
                    }
                        
                    ?>
                    
            </div>

		</div>


        </div>
    </div>
</div>


    </main>



@endsection