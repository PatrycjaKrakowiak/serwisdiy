@extends('layouts.masterE')

@section('content')
 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> 
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/accountE"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/accountProjectE"><i class="fa fa-book fa-fw"></i>Added prjects</a></li>
                <li><a href="/addProjectE"><i class="fa fa-plus fa-fw"></i>Add project</a></li>
                <li><a href="/loveProjectE"><i class="fa fa-heart fa-fw"></i>Favourite projects</a></li>
                <li  class="active"><a href="changeInfoE"><i class="fa fa-pencil fa-fw"></i>Edit account</a></li>
                <li><a href="changePassE"><i class="fa fa-cogs fa-fw"></i>Change password</a></li>
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
            <div class="col-md-2">
            
        	</div>

        {{-- 	<div class="col-md-3">
        		
			
        	</div> --}}

		</div>

        <?php

                $log = Auth::user()->login;
            
                    foreach ($userName as $value ) 
                    {
                        if ($value->login == $log)
                        {

                        
        ?>
        <div>

<div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
           <center><h3>Change information</h3></center>   
   
      {!! Form::model($userName, array('method'=>'PATCH', 'route' =>array('users.update', $value->id))) !!}

             <div class="form-group">

            {!! Form::label('imie', 'Name', ['class' =>'form-label']) !!}

            {!!  Form::text('imie', $value->imie, ['class' =>'form-control', 'required']) !!}
            </div>


            <div class="form-group">
            {!! Form::label('nazwisko', 'Surname') !!}
            {!! Form::text('nazwisko',  $value->nazwisko, ['class' =>'form-control', 'required']) !!} 
            </div>

            <div class="form-group">
            {!! Form::label('email','E-mail') !!}
            {!! Form::email('email',  $value->email, ['class' =>'form-control', 'required']) !!} 
            </div>

            <div class="form-group">
            {!! Form::label('dataUr', 'Birth date') !!}
            {!! Form::date('dataUr',  $value->dataUr, ['class' =>'form-control', 'required']) !!} 
            </div>
<center>
            {!! Form::token() !!}
            {!! Form::submit('Edit', array('class' =>'btn btn-default')) !!}
 </center>           
            </form>
            </div>
          
        {!! Form::close() !!}
<?php
            }
                    }
                

            ?>
</div>
        </div>
    </div>
</div>


    </main>



@endsection