@extends('layouts.masterE')

@section('content')

{{ \Auth::user()->name}}


<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
         <br> <br> <br>
  <div class="row">
  <div class="span3">
    <div class="well">
        <div>
            <ul class="nav nav-list">
                <li><label class="tree-toggle nav-header">Children</label>
                    <ul class="nav nav-list tree">
                          <li><a href="/projectsENG/projectCategoryE/ubranka">Clothes</a></li>
                          <li><a href="/projectsENG/projectCategoryE/zabawki">Toys</a></li>
                          <li><a href="/projectsENG/projectCategoryE/akcesoria">Accesories</a></li>
                      <!--   <li><label class="tree-toggle nav-header">Next</label>
                            <ul class="nav nav-list tree">
                                <li><a href="#">Next</a></li>
                                <li><a href="#">Next</a></li>
                                <li><label class="tree-toggle nav-header">Next</label>
                                    <ul class="nav nav-list tree">
                                        <li><a href="#">Next</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Home</label>
                    <ul class="nav nav-list tree">
                         <li><a href="/projects/projectCategory/meble">Furniture</a></li>
                         <li><a href="/projects/projectCategory/tekstylia">Textil</a></li>
                         <li><a href="/projects/projectCategory/ogród">Garden</a></li>
                         <li><a href="/projects/projectCategory/dekoracje">Decoration</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Women clothes</label>
                    <ul class="nav nav-list tree">
                     <li><a href="/projects/projectCategory/dbluzki">Shirts</a></li>
                     <li><a href="/projects/projectCategory/dspodnie">Trousers</a></li>
                     <li><a href="/projects/projectCategory/dsukienki">Dresses</a></li>
                     <li><a href="/projects/projectCategory/dspódnice">Skirts</a></li>
                     <li><a href="/projects/projectCategory/dwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Men clothes</label>
                    <ul class="nav nav-list tree">
                       <li><a href="/projects/projectCategory/mkoszulki">T-shirts</a></li>
                       <li><a href="/projects/projectCategory/mspodnie">Trousers</a></li>
                       <li><a href="/projects/projectCategory/mkoszule">Shirt</a></li>
                       <li><a href="/projects/projectCategory/mmarynarki">Jacets</a></li>
                      <li><a href="/projects/projectCategory/mwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    </div>
</div>

<script type="text/javascript">
  $('.tree-toggle').click(function () {
 $(this).parent().children('ul.tree').toggle(200);
    });
  $(function(){
  $('.tree-toggle').parent().children('ul.tree').toggle(200);
  })
</script> 
<!-- koniec -->

    </div>   <!-- koniec pierwszej kolumny rozm 2 -->

    <div class="col-md-6"> <!-- pierwszy wiersz, kolumna druga rozm 7 -->
      
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-2">
                 <!-- 
                 -->
              </div>
              <div class="col-md-8">

                <h3> New added projects </h3>
                <br>
                 <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                      </ol>

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">

                        <div class="item active">
                          <img src="img/newProject/zajac.png" alt="Chania" >
                          <div class="carousel-caption">
                            <h3>Dekoracyjny zajączek</h3>
                          </div>
                        </div>

                        <div class="item">
                          <img src="img/newProject/b1.png" alt="Chania" >
                          <div class="carousel-caption">
                            <h3>Bluzka raglanowa</h3>
                          </div>
                        </div>
                      
                        <div class="item">
                          <img src="img/newProject/body.png" alt="Flower" >
                          <div class="carousel-caption">
                            <h3>Body dziewczęce</h3>
                          </div>
                        </div>

                        <div class="item">
                          <img src="img/newProject/minion.png" alt="Flower" >
                          <div class="carousel-caption">
                            <h3>Maskotka Minionek</h3>
                          </div>
                        </div>

                    {{--     <div class="item">
                          <img src="img/newProject/piornik.png" alt="Flower">
                          <div class="carousel-caption">
                            <h3>Piórnik z kokardką</h3>
                          </div>
                        </div> --}}
                    
                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div> <!-- finish carousel slide -->

                 <div class="row">
            <br><br>
                  <h3>The best projects</h3>
                </div>
                <div class='row'>
                   <div class="col-md-2">
                 <!-- 
                 -->
              </div>
                  <div class='col-md-8'>
                    <div class="carousel slide media-carousel" id="media">
                      <div class="carousel-inner">
                        <div class="item  active">
                          <div class="row">
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/zajaczki1.jpg"></a>
                            </div>          
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/zajaczki2.jpg"></a>
                            </div>
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/zajaczki3.jpg"></a>
                            </div>        
                          </div>
                        </div>
                        <div class="item">
                          <div class="row">
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/etuichoinki.jpg"></a>
                            </div>          
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/etuigwiazdki.jpg"></a>
                            </div>
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/wzory.jpg"></a>
                            </div>        
                          </div>
                        </div>
                        <div class="item">
                          <div class="row">
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/bruno2.jpg"></a>
                            </div>          
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/brunop2.jpg"></a>
                            </div>
                            <div class="col-md-4">
                              <a class="thumbnail" href="#"><img alt="" src="img/bestProject/lilka.jpg"></a>
                            </div>      
                          </div>
                        </div>
                      </div>
                      <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                      <a data-slide="next" href="#media" class="right carousel-control">›</a>
                    </div>                          
                  </div>
                </div>
                </div>

                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


</main>

@endsection