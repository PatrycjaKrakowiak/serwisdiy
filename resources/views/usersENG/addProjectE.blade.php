@extends('layouts.masterE')

@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif


 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> 
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/accountE"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/accountProjectE"><i class="fa fa-book fa-fw"></i>Added projects</a></li>
                <li class="active"><a href="/addProjectE"><i class="fa fa-plus fa-fw"></i>Add project</a></li>
                <li><a href="/loveProjectE"><i class="fa fa-heart fa-fw"></i>Favourite projects</a></li>
                <li><a href="/changeInfoE"><i class="fa fa-pencil fa-fw"></i>Edit account</a></li>
                <li><a href="/exitchangePassE"><i class="fa fa-cogs fa-fw"></i>Change password</a></li>
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
            <div class="col-md-2">
            
        	</div>

        	<div class="col-md-3">
        		
			
        	</div>

		</div>

        	<h2> <center> Add project </center></h2>
           {{--  {!! Form::open(array('route' =>'data.store')) !!} --}}
 {!! Form::open(array('class'=>'form-horizontal', 'files'=>true)) !!}


            <div class="form-group">

            {!! Form::label('nameProject', 'Name', ['class' =>'col-form-label']) !!}

            {!!  Form::text('nameProject', null, ['class' =>'form-control', 'placeholder'=>'Title']) !!}
            </div>


            <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' =>'form-control', 'placeholder'=>'Project description']) !!} 
            </div>

            {!! Form::label('subCategory', 'Category') !!}
            {!! Form::select('subCategory', array(
                    'Children' => array('ubranka' => 'Clothes',
                                      'zabawki' => 'Toys',
                                      'akcesoria' => 'Accesories'),

                    'Home' => array( 'meble' => 'Furniture',
                                    'tekstylia' => 'Textil',
                                    'ogród' => 'Garden',
                                    'dekoracje' => 'Decoration'),

                    'Women clothes' => array('dbluzki' => 'Shirts',
                                             'dspodnie' => 'Trousers',
                                             'dsukienki' => 'Dresses',
                                             'dspódnice' => 'Skirts',
                                             'dwierzchnia' => 'Overclothes'),

                    'Men clothes' => array( 'mkoszulki' => 'T-shirts',
                                             'mspodnie' => 'Trousers',
                                             'mkoszule' => 'Shirts',
                                             'mmarynarki' => 'Jackets',
                                             'mwierzchnia' => 'Overclothes'),
                )); 
            !!}

            {!! Form::file('image', array('multiple'=>false)) !!}
            {!! Form::file('file', array('multiple'=>false)) !!}
<br>
            {!! Form::token() !!}
            {!! Form::submit('Add project', array('class' =>'btn btn-default')) !!}
            
        {!! Form::close() !!}


        </div>
    </div>
</div>


    </main>



@endsection