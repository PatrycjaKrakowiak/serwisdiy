<!DOCTYPE html>

<head>
    <meta charset="utf-8">

   
    <title>ZróbToSam</title>
    

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&subset=latin,latin-ext" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">


    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ URL::asset('/css/bootstrap-theme.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/bootstrap-theme.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/m.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/register.css')}}" rel="stylesheet">
   
    <link href="{{ URL::asset('/css/contact.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/grid.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/socialicon.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/css/media-carousel.css')}}" rel="stylesheet">


    <script src="js/category.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body>

     <header class="header">
<?php 
  if(Auth::check()){

  ?>
              <div class="container">
                <nav class="navbar1 navbar-default navbar-fixed-top">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar5">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                     
                    </div>
                    <div id="navbar5" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="/account"><span class="glyphicon glyphicon-user"></span> Moje konto</a></li>
                        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> Wyloguj się</a></li>
                      </ul>
                    </div>
                    <!--/.nav-collapse -->
                  </div>
                  <!--/.container-fluid -->
                </nav>

              </div>

<?php
}
else
{
?>
   <div class="container">
                <nav class="navbar1 navbar-default navbar-fixed-top">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar5">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                     
                    </div>
                    <div id="navbar5" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="/users/create"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
                        <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Zaloguj się</a></li>
                      </ul>
                    </div>
                    <!--/.nav-collapse -->
                  </div>
                  <!--/.container-fluid -->
                </nav>

              </div>
<?php
}
?>
<center>
      <br>   
  <a href="home">  
    <img src="/../img/logo.png" class="img-responsive" width="750" height="400"> 
  </a>
<hr>
</center>

    </header>

    <!-- wrapper -->
    <div class="site-wrappper">

        <!-- .container -->
        <div class="container site-content">

{{--       <ul class="nav nav-pills">
      @if(\Auth::check())

      <li>
        {{link_to_route('logout', 'Logout')}}
      </li>
      @else
          <li>
        {{link_to_route('login', 'Login')}}
      </li>

      </ul>

      @endif

       <li>
        {{link_to_route('users.create', 'Nowy uzytkownik')}}
      </li> --}}
            @yield('content')
            
        </div><!-- end of .container -->
        
    </div><!-- end of wrapper -->
    

    <!-- Footer -->
   
    <footer class="footer">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- // -->   
  <hr />
<div class="container">
 
        <div class="text-center center-block">
        <p class="foot">
              <a href="#"><i class="foot1">Informacje</i></a>
              <a href="/../contact"><i class="foot1">Kontakt</i></a>
              <a href="#"><i class="foot1">Regulamin</i></a>
              <a href="homeE"> <i class="foot1">English</i></a>
         </p>
        </div>

        <div class="text-center center-block">
              <a href="#"><i class="fa fa-facebook-square fa-3x social"></i></a>
              <a href="#"><i class="fa fa-google-plus-square fa-3x social"></i></a>
              <a href="#"><i class="fa fa-envelope-square fa-3x social"></i></a>
        </div>
   
</div>
  <hr />
    </footer>


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
