@extends('layouts.masterE')

@section('content')
 

<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
            <br> <br> <br>
  <div class="row">
  <div class="span3">
    <div class="well">
        <div>
            <ul class="nav nav-list">
                <li><label class="tree-toggle nav-header">Children</label>
                    <ul class="nav nav-list tree">
                          <li><a href="/projectsENG/projectCategoryE/ubranka">Clothes</a></li>
                          <li><a href="/projectsENG/projectCategoryE/zabawki">Toys</a></li>
                          <li><a href="/projectsENG/projectCategoryE/akcesoria">Accesories</a></li>
                      <!--   <li><label class="tree-toggle nav-header">Next</label>
                            <ul class="nav nav-list tree">
                                <li><a href="#">Next</a></li>
                                <li><a href="#">Next</a></li>
                                <li><label class="tree-toggle nav-header">Next</label>
                                    <ul class="nav nav-list tree">
                                        <li><a href="#">Next</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Home</label>
                    <ul class="nav nav-list tree">
                         <li><a href="/projectsENG/projectCategoryE/meble">Furniture</a></li>
                         <li><a href="/projectsENG/projectCategoryE/tekstylia">Textil</a></li>
                         <li><a href="/projectsENG/projectCategoryE/ogród">Garden</a></li>
                         <li><a href="/projectsENG/projectCategoryE/dekoracje">Decoration</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Women clothes</label>
                    <ul class="nav nav-list tree">
                     <li><a href="/projects/projectCategory/dbluzki">Shirts</a></li>
                     <li><a href="/projects/projectCategory/dspodnie">Trousers</a></li>
                     <li><a href="/projects/projectCategory/dsukienki">Dresses</a></li>
                     <li><a href="/projects/projectCategory/dspódnice">Skirts</a></li>
                     <li><a href="/projects/projectCategory/dwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Men clothes</label>
                    <ul class="nav nav-list tree">
                       <li><a href="/projects/projectCategory/mkoszulki">T-shirts</a></li>
                       <li><a href="/projects/projectCategory/mspodnie">Trousers</a></li>
                       <li><a href="/projects/projectCategory/mkoszule">Shirt</a></li>
                       <li><a href="/projects/projectCategory/mmarynarki">Jacets</a></li>
                      <li><a href="/projects/projectCategory/mwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    </div>
</div>

<script type="text/javascript">
  $('.tree-toggle').click(function () {
 $(this).parent().children('ul.tree').toggle(200);
    });
  $(function(){
  $('.tree-toggle').parent().children('ul.tree').toggle(200);
  })
</script> 
<!-- koniec -->

    </div>   <!-- koniec pierwszej kolumny rozm 2 -->

    <div class="col-md-6"> <!-- pierwszy wiersz, kolumna druga rozm 7 -->
   
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-1">
                 <!-- 
                 -->
              </div>
              <div class="">

              {{-- FORMULARZ KONTAKTOWY --}}

<div class="container">
	<div class="row">
      <div class="col-md-5 col-md-offset-3">
        <div class="">
         
          <fieldset>
            <legend class="text-center">Write to us</legend>

    {!! Form::open(array('class'=>'form-horizontal')) !!}
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Name</label>
              <div class="col-md-8">
               {!!  Form::text('name', null, ['class' =>'col-md-8 form-control', 'placeholder'=>'Name', 'required']) !!}
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Your E-mail</label>
              <div class="col-md-8">
               {!!  Form::text('email', null, ['class' =>'col-md-8 form-control', 'placeholder'=>'Your e-mail', 'required']) !!}
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Message</label>
              <div class="col-md-8">
                  {!! Form::textarea('message', null, ['class' =>'form-control', 'placeholder'=>'Message', 'required']) !!} 
              </div>
            </div>
    
            <!-- Form actions -->
    
           {!! Form::submit('Send', array('class' =>'btn btn-primary btn-lg')) !!}
            
        {!! Form::close() !!}
          </fieldset>

        

        </div>
      </div>
	</div>
</div>


                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


</main>

@endsection