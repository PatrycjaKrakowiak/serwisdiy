@extends('layouts.masterE')

@section('content')
 

<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
           <br> <br> <br>
  <div class="row">
  <div class="span3">
    <div class="well">
        <div>
            <ul class="nav nav-list">
                <li><label class="tree-toggle nav-header">Children</label>
                    <ul class="nav nav-list tree">
                          <li><a href="/projects/projectCategory/ubranka">Clothes</a></li>
                          <li><a href="/projects/projectCategory/zabawki">Toys</a></li>
                          <li><a href="/projects/projectCategory/akcesoria">Accesories</a></li>
                      <!--   <li><label class="tree-toggle nav-header">Next</label>
                            <ul class="nav nav-list tree">
                                <li><a href="#">Next</a></li>
                                <li><a href="#">Next</a></li>
                                <li><label class="tree-toggle nav-header">Next</label>
                                    <ul class="nav nav-list tree">
                                        <li><a href="#">Next</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Home</label>
                    <ul class="nav nav-list tree">
                         <li><a href="/projects/projectCategory/meble">Furniture</a></li>
                         <li><a href="/projects/projectCategory/tekstylia">Textil</a></li>
                         <li><a href="/projects/projectCategory/ogród">Garden</a></li>
                         <li><a href="/projects/projectCategory/dekoracje">Decoration</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Women clothes</label>
                    <ul class="nav nav-list tree">
                     <li><a href="/projects/projectCategory/dbluzki">Shirts</a></li>
                     <li><a href="/projects/projectCategory/dspodnie">Trousers</a></li>
                     <li><a href="/projects/projectCategory/dsukienki">Dresses</a></li>
                     <li><a href="/projects/projectCategory/dspódnice">Skirts</a></li>
                     <li><a href="/projects/projectCategory/dwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Men clothes</label>
                    <ul class="nav nav-list tree">
                       <li><a href="/projects/projectCategory/mkoszulki">T-shirts</a></li>
                       <li><a href="/projects/projectCategory/mspodnie">Trousers</a></li>
                       <li><a href="/projects/projectCategory/mkoszule">Shirt</a></li>
                       <li><a href="/projects/projectCategory/mmarynarki">Jacets</a></li>
                      <li><a href="/projects/projectCategory/mwierzchnia">Overclothes</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
  $('.tree-toggle').click(function () {
 $(this).parent().children('ul.tree').toggle(200);
    });
  $(function(){
  $('.tree-toggle').parent().children('ul.tree').toggle(200);
  })
</script> 
<!-- koniec -->

    </div>   <!-- koniec pierwszej kolumny rozm 2 -->

    <div class="col-md-6"> <!-- pierwszy wiersz, kolumna druga rozm 7 -->
   
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-2">
                 <!-- 
                 -->
              </div>
              <div class="col-md-8">
  <div class="valid">
  @if ($errors->any())      
    @foreach( $errors->all() as $message )
      <div class="alert alert-info">
         {{ $message }}
      </div>  
    @endforeach        
  @endif
</div>

       <?php
                
                 
                    foreach ($projectName as $value) 
                    {
                      
                        if ($value->idProject == $idProject)
                        {
                            $foto = $value->fotoProject;
                            $idFoto = $value->idProject;
                            $tytulFoto = $value ->nameProject;
                            $opis = $value->description;
                            $fileName = $value->fileProject;
                            
                        ?>
                     <h3>    {{$tytulFoto}} </h3>
                       <br>

                     <img src="/upload/uploadPhoto/{{$foto}}" alt="projekt" width="320" height="240">
                       <br>
                       <br>
                       {{$opis}}
                       <br>
                       <br>
                       <a href="/upload/uploadFile/{{$fileName}}" download="{{$fileName}}">
                     	 <button type="button" class="btn btn-primary">
                     	 <i class="glyphicon glyphicon-download"> Download </i></button>
					             </a>

    <br><br>
                       
                       <?php
                       }
                       
                    }
                    
?>


<?php     
                  $licznik =0;
                  if(Auth::user()!=null)
                  {
                  $log = Auth::user()->id;
                 
                  $result = DB::select('select * from favourite where idU = :id', ['id' => $log]);
                       foreach ($result as $value2) {
                      
                        if ($value2->idP==$idProject && $value2->idU==$log) {
                          $licznik++;
                           $idF=$value2->idF;
                    
                        }
                       
                        }
                      if($licznik==1)
                      {
                        ?>
                        <a href="/projects/aboutProject/{{$idProject}}/deleteF">Remove from favourite</a>
                        <br><br>
                        <?php
                      }

                      else {
                        ?>

                          <a href="/projects/aboutProject/{{$idProject}}/addF">Add to favourite</a> 
                          <br><br>  
                          <?php
                      }
?>
<br>
<h3>Add your note:</h3>
    <div class="gradeBox">
        {{ Form::open(array('method' => 'post' , 'class' => 'form-inline' )) }} 

          <div class="form-group" style="width:100%; position:relative">                             
              {{ Form::select('grade', array('1' => '1', '1.5' => '1.5',
                                              '2' => '2', '2.5' => '2.5',
                                              '3' => '3', '3.5' => '3.5',
                                              '4' => '4', '4.5' => '4.5',
                                              '5' => '5')) }}
          </div>
          <div class="form-group">                
              {{ Form::submit('Add', array('class' => 'btn btn-block btn-primary' , 'style' => 'width:100px')) }}
          </div>
        {{ Form::close() }}         
    </div>
    <?php
}
?>                           
    <br><br>

  <div class="actionBox2">
      {{--  @if (!empty($grades)) --}}
          <h3>Avarage of project:</h3>
          <?php $count=0; 
                $suma=0;
               
                ?>

              <div class="commentAnswerBox" style="background-color:#ade5f4"></div>
                          
                <div class="commentText"> 
               <?php
                $sum = DB::select('select * from grade where idP = :id', ['id' => $idProject]);

                    foreach ($sum as $val)
                     {
                      $suma= $suma + $val->valueGrade;    
                      $count++;  

                     } 
                     if ($count>0) {
                        $avarage=$suma/$count;
                        echo $avarage;
                      }
                      else{
                        $avarage="Project has not any notes.";
                        echo $avarage;
                      }
                      ?>
                     </div>
          
       
         
  </div> 
<?php
if (Auth::user()!=null) {
?>
  <br><br>
<h3>Add comment</h3>
 
    <div class="actionBox">
        {{ Form::open(array( 'method' => 'post' , 'class' => 'form-inline' )) }} 

          <div class="form-group" style="width:100%; position:relative">                             
              {{ Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Write text', 'rows' => '4', 'required']) }}
          </div>
          <div class="form-group">                
              {{ Form::submit('Add comment', array('class' => 'btn btn-block btn-primary' , 'style' => 'width:220px')) }}
          </div>
        {{ Form::close() }}         
    </div>

<?php
}
?>
     <div class="actionBox">
                    
          @if (!empty($comments))
          <h3>Added comments:</h3>
            @foreach ($comments as $cm)
              <div class="commentAnswerBox" style="background-color:#ade5f4"></div>
                          
                <div class="commentText"> 
               <?php
                $whoC = DB::select('select * from users where id = :idC', ['idC' => $cm->idU]);
                ?>                     
                    <p class="" >
                    <?php
                    foreach ($whoC as $val) {
                      $whoC2= $val->login;                    

                    ?>
                    {{$whoC2}} <?php } ?>
                     {{$cm->comment}}</p> 
                    <div style="margin-top:10px">                    
                                         
                    </div>
                </div>
                           
            @endforeach  
          @endif
             
    </div>


</div>



             
                </div>


                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


</main>

@endsection