@extends('layouts.master')

@section('content')
 

<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
     <br> <br> <br>
   <br> <br> <br>
  <div class="row">
  <div class="span3">
    <div class="well">
        <div>
            <ul class="nav nav-list">
                <li><label class="tree-toggle nav-header">Dzieci</label>
                    <ul class="nav nav-list tree">
                          <li><a href="/projects/projectCategory/ubranka">Ubranka</a></li>
                          <li><a href="/projects/projectCategory/zabawki">Zabawki</a></li>
                          <li><a href="/projects/projectCategory/akcesoria">Akcesoria</a></li>
                      <!--   <li><label class="tree-toggle nav-header">Next</label>
                            <ul class="nav nav-list tree">
                                <li><a href="#">Next</a></li>
                                <li><a href="#">Next</a></li>
                                <li><label class="tree-toggle nav-header">Next</label>
                                    <ul class="nav nav-list tree">
                                        <li><a href="#">Next</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Dom</label>
                    <ul class="nav nav-list tree">
                         <li><a href="/projects/projectCategory/meble">Meble</a></li>
                         <li><a href="/projects/projectCategory/tekstylia">Tekstylia</a></li>
                         <li><a href="/projects/projectCategory/ogród">Ogród</a></li>
                         <li><a href="/projects/projectCategory/dekoracje">Dekoracje</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Odzież damska</label>
                    <ul class="nav nav-list tree">
                     <li><a href="/projects/projectCategory/dbluzki">Bluzki</a></li>
                     <li><a href="/projects/projectCategory/dspodnie">Spodnie</a></li>
                     <li><a href="/projects/projectCategory/dsukienki">Sukienki</a></li>
                     <li><a href="/projects/projectCategory/dspódnice">Spódnice</a></li>
                     <li><a href="/projects/projectCategory/dwierzchnia">Odzież wierzchnia</a></li>

                    </ul>
                </li>

                <li class="divider"></li>
                <li><label class="tree-toggle nav-header">Odzież męska</label>
                    <ul class="nav nav-list tree">
                       <li><a href="/projects/projectCategory/mkoszulki">Koszulki</a></li>
                       <li><a href="/projects/projectCategory/mspodnie">Spodnie</a></li>
                       <li><a href="/projects/projectCategory/mkoszule">Koszule</a></li>
                       <li><a href="/projects/projectCategory/mmarynarki">Marynarki</a></li>
                      <li><a href="/projects/projectCategory/mwierzchnia">Odzież wierzchnia</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    </div>
</div>

<script type="text/javascript">
  $('.tree-toggle').click(function () {
 $(this).parent().children('ul.tree').toggle(200);
    });
  $(function(){
  $('.tree-toggle').parent().children('ul.tree').toggle(200);
  })
</script> 
<!-- koniec -->

    </div>   <!-- koniec pierwszej kolumny rozm 2 -->

    <div class="col-md-6"> <!-- pierwszy wiersz, kolumna druga rozm 7 -->
   
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-1">
                 <!-- 
                 -->
              </div>
              <div class="">

              {{-- FORMULARZ KONTAKTOWY --}}

<div class="container">
	<div class="row">
      <div class="col-md-5 col-md-offset-3">
        <div class="">
         
          <fieldset>
            <legend class="text-center">Napisz do nas</legend>

    {!! Form::open(array('class'=>'form-horizontal')) !!}
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Imię</label>
              <div class="col-md-8">
               {!!  Form::text('name', null, ['class' =>'col-md-8 form-control', 'placeholder'=>'Imię', 'required']) !!}
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Twój E-mail</label>
              <div class="col-md-8">
               {!!  Form::text('email', null, ['class' =>'col-md-8 form-control', 'placeholder'=>'Twój e-mail', 'required']) !!}
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Wiadomość</label>
              <div class="col-md-8">
                  {!! Form::textarea('message', null, ['class' =>'form-control', 'placeholder'=>'Wiadomość', 'required']) !!} 
              </div>
            </div>
    
            <!-- Form actions -->
    
           {!! Form::submit('Wyślij', array('class' =>'btn btn-primary btn-lg')) !!}
            
        {!! Form::close() !!}
          </fieldset>

        

        </div>
      </div>
	</div>
</div>


                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


</main>

@endsection