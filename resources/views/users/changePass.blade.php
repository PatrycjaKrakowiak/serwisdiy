@extends('layouts.master')

@section('content')
 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> 
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/account"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/accountProject"><i class="fa fa-book fa-fw"></i>Dodane projekty</a></li>
                <li><a href="/addProject"><i class="fa fa-plus fa-fw"></i>Dodaj projekt</a></li>
                <li><a href="/loveProject"><i class="fa fa-heart fa-fw"></i>Ulubione projekty</a></li>
                <li><a href="changeInfo"><i class="fa fa-pencil fa-fw"></i>Edytuj dane</a></li>
                <li class="active"><a href="changePass"><i class="fa fa-cogs fa-fw"></i>Zmień hasło</a></li>
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
            <div class="col-md-2">
            
        	</div>
</div>

        <?php

                $log = Auth::user()->login;
            
                    foreach ($userName as $value ) 
                    {
                        if ($value->login == $log)
                        {

                        
        ?>
        <div>

<div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
           <center><h3>Zmień hasło</h3></center>   
   
      {!! Form::model($userName, array('method'=>'PATCH', 'route' =>array('users.updatePass', $value->id))) !!}

          <div class="form-group">
            {!! Form::label('password', 'Hasło') !!}
            {!! Form::password('password', ['class' =>'form-control', 'placeholder'=>'Wprowadź nowe hało']) !!} 
        </div>
<center>
            {!! Form::token() !!}
            {!! Form::submit('Edytuj', array('class' =>'btn btn-default')) !!}
 </center>           
            </form>
            </div>
          
        {!! Form::close() !!}
<?php
            }
                    }
                

            ?>
</div>
        </div>
    </div>
</div>



    </main>



@endsection