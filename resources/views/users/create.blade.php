@extends('layouts.master')

@section('content')



@if(count($errors))
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>

@endif


<main class="row main-content">
      
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->

    </div>   <!-- koniec pierwszej kolumny rozm 1 -->

    <div class="col-md-8"> <!-- pierwszy wiersz, kolumna druga rozm 8 -->
      
        <div class="container bg">

          <div class="row">
               
              <div class="col-md-8">
    <!-- Tu wpisz treśc strony -->

  <div class="col-sm-6 col-md-4 col-md-offset-4 ">
<h2>Rejestracja</h2>
{!! Form::open(array('route' =>'users.store')) !!}



	<div class="form-group">

	{!! Form::label('imie', 'Imię', ['class' =>'col-form-label']) !!}

	{!!  Form::text('imie', null, ['class' =>'form-control', 'placeholder'=>'Imię']) !!}
	</div>


	<div class="form-group">
	{!! Form::label('nazwisko', 'Nazwisko') !!}
	{!! Form::text('nazwisko', null, ['class' =>'form-control', 'placeholder'=>'Nazwisko']) !!}	
	</div>

	<div class="form-group">
	{!! Form::label('email','E-mail') !!}
	{!! Form::email('email', null, ['class' =>'form-control', 'placeholder'=>'E-mail']) !!}	
	</div>

	<div class="form-group">
	{!! Form::label('dataUr', 'Data urodzenia') !!}
	{!! Form::date('dataUr', null, ['class' =>'form-control', 'placeholder'=>'Data urodzenia']) !!}	
	</div>

	<div class="form-group">
	{!! Form::label('login', 'Login') !!}
	{!! Form::text('login', null, ['class' =>'form-control', 'placeholder'=>'Login']) !!}	
	</div>

	<div class="form-group">
	{!! Form::label('password', 'Hasło') !!}
	{!! Form::password('password', ['class' =>'form-control', 'placeholder'=>'Wprowadż min. 6 znaków']) !!}	
	</div>


{{-- 	<div class="form-group">
	{!! Form::label('password2') !!}
	{!! Form::password('password', ['class' =>'form-control', 'placeholder'=>'Powtórz hasło']) !!}	
	</div> --}}

	{!! Form::token() !!}
	{!! Form::submit(null, array('class' =>'btn btn-default')) !!}
	
{!! Form::close() !!}

          </div>
           

                  </div> <!-- finish col 8 -->

                      </div>

                      </div> <!-- finish container bg -->

  <!-- -->
              

  <!-- -->

    </div> <!-- koniec drugiej kolumny pierwszego wiersza (col7) -->
  
  </div>
 
</div>


@endsection