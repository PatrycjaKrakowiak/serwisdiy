@extends('layouts.master')

@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif


 <link href="{{ URL::asset('/css/clientpage.css')}}" rel="stylesheet">
  <main class="row main-content">
 
    <div class="row justify-content-md-center">
    <div class="col-md-2">
      <!--
 Wprowadzone zmiany
      -->
      </div>
     <br> 
            <div class="row">
         
        <div class="col-md-3 well">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/account"><i class="fa fa-home fa-fw"></i>Home</a></li>
                <li><a href="/accountProject"><i class="fa fa-book fa-fw"></i>Dodane projekty</a></li>
                <li class="active"><a href="/addProject"><i class="fa fa-plus fa-fw"></i>Dodaj projekt</a></li>
                <li><a href="/loveProject"><i class="fa fa-heart fa-fw"></i>Ulubione projekty</a></li>
                <li><a href="/changeInfo"><i class="fa fa-pencil fa-fw"></i>Edytuj dane</a></li>
                <li><a href="/exitchangePass"><i class="fa fa-cogs fa-fw"></i>Zmień hasło</a></li>
            </ul>
        </div>

		 <div class="col-md-1">
		      <!--
		 Wprowadzone zmiany
		      -->
		      </div>


        <div class="col-md-8 well">

        <div class="row">
           
            <div class="col-md-2">
            
        	</div>

        	<div class="col-md-3">
        		
			
        	</div>

		</div>

        	<h2> <center> Dodaj projekt </center></h2>
           {{--  {!! Form::open(array('route' =>'data.store')) !!} --}}
 {!! Form::open(array('class'=>'form-horizontal', 'files'=>true)) !!}


            <div class="form-group">

            {!! Form::label('nameProject', 'Nazwa', ['class' =>'col-form-label']) !!}

            {!!  Form::text('nameProject', null, ['class' =>'form-control', 'placeholder'=>'Tytuł projektu']) !!}
            </div>


            <div class="form-group">
            {!! Form::label('description', 'Opis') !!}
            {!! Form::textarea('description', null, ['class' =>'form-control', 'placeholder'=>'Opis projektu']) !!} 
            </div>

            {!! Form::label('subCategory', 'Kategoria') !!}
            {!! Form::select('subCategory', array(
                    'Dzieci' => array('ubranka' => 'Ubranka',
                                      'zabawki' => 'Zabawki',
                                      'akcesoria' => 'akcesoria'),

                    'Dom' => array( 'meble' => 'Meble',
                                    'tekstylia' => 'Tekstylia',
                                    'ogród' => 'Ogród',
                                    'dekoracje' => 'Dekoracje'),

                    'Odzież damska' => array('dbluzki' => 'Bluzki',
                                             'dspodnie' => 'Spodnie',
                                             'dsukienki' => 'Sukienki',
                                             'dspódnice' => 'Spódnice',
                                             'dwierzchnia' => 'Odzież wierzchnia'),

                    'Odzież męska' => array( 'mkoszulki' => 'Koszulki',
                                             'mspodnie' => 'Spodnie',
                                             'mkoszule' => 'Koszule',
                                             'mmarynarki' => 'Marynarki',
                                             'mwierzchnia' => 'Odzież wierzchnia'),
                )); 
            !!}

            {!! Form::file('image', array('multiple'=>false)) !!}
            {!! Form::file('file', array('multiple'=>false)) !!}
<br>
            {!! Form::token() !!}
            {!! Form::submit('Dodaj projekt', array('class' =>'btn btn-default')) !!}
            
        {!! Form::close() !!}


        </div>
    </div>
</div>


    </main>



@endsection