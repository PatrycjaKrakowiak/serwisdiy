<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryWomen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_women', function (Blueprint $table) {
            $table->increments('idProject');
            $table->string('subCategory');
            $table->string('nameProject');
            $table->string('description');
            $table->string('author');
            $table->string('fotoProject');
            $table->string('fileProject');
            $table->double('gradeProject');
          
           
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('category_women');
    }
}
