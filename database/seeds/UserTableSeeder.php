<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('users')->delete();
       User::create([
       	    'login' => 'Janina123',
            'password' => bcrypt('janina123'),
            'imie' => 'Janina',
            'nazwisko' => 'Kowalska',
            'email' => 'janina@gmail.com',
            'dataUr' => '1978-02-03'
       	]);
    }
}
