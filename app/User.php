<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imie', 'nazwisko','email', 'dataUr','login','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $login_validation_rules = [
        'login' => 'required|exists:users',
        'password' => 'required'
    ];

    public static $create_validation_rules = [
        'imie' => 'required',
        'nazwisko' => 'required',
        'email' => 'required|unique:users',
        'dataUr'  => 'required|date',
        'login' => 'required|unique:users',
        'password' => 'required|min"6'
    ];

     public static $editInfo_validation_rules = [
        'imie' => 'required',
        'nazwisko' => 'required',
        'email' => 'required|unique:users',
        'dataUr'  => 'required|date'
    ];

     public static $editPass_validation_rules = [
    
        'login' => 'required|unique:users',
        'password' => 'required|min"6'
    ];


}
