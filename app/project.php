<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    protected $fillable = ['nameProject', 'description', 'subCategory', 'author', 'fotoProject', 'fileProject', 'gradeProject'];

     public static $create_validation_rules = [
        'nameProject' => 'required',
        'description' => 'required',
        'image' => 'image|max:50000' 
    ];
}
