<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;

class UsersControllerE extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('usersENG.createE');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, User::$create_validation_rules);
       $data = $request->only('login', 'password','imie', 'nazwisko','email', 'dataUr');
       $data['password'] = bcrypt($data['password']);
       $user = User::create($data);
       if($user)
       {
        \Auth::login($user);
  
        return redirect()->route('homeE');
       }

       return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user=User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        // $this->validate($request, ['imie' => 'required',
        // 'nazwisko' => 'required',
        // 'email' => 'required|unique:users',
        // 'dataUr'  => 'required|date',]);

        $user = User::find($id);

    //    $user= myform::findOrFail($id);
       // $this->validate($user, User::$editInfo_validation_rules);
        $user->imie = Input::get('imie');
        $user->nazwisko = Input::get('nazwisko');
        $user->email = Input::get('email');
        $user->dataUr = Input::get('dataUr');

        $user->save();

        return redirect('accountE');

    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

      public function home()
    {
        return view('usersENG.homeE');
    }

     public function account()
    {
        return view('usersENG.accountE');
    }
}
