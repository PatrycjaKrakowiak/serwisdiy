<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use App\User;
use App\project;
use File;

use Illuminate\Http\Request;

class AdminControllerE extends Controller
{
      public function showAdmin()
    {
        return view('adminENG.adminE');
    }

        public function showAllP()
    {
    	$userProject = DB::table('projects')->get();
        return view('adminENG.allProjectsE',compact('userProject'));
    }

        public function showAllU()
    {
    	$userName = DB::table('users')->get();
        return view('adminENG.allUsersE', compact('userName'));
    }

        public function showStats()
    {
        return view('adminENG.statsE');
    }
}
