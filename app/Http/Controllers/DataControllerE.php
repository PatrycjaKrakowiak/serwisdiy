<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use App\User;
use App\project;
use File;

use Illuminate\Http\Request;

class DataControllerE extends Controller
{
   	public function showName()
	{
		$userName = DB::table('users')->get();
		return view('usersENG.accountE', compact('userName'));
	}

		public function showInfo()
	{
		$userName = DB::table('users')->get();
		return view('usersENG.changeInfoE', compact('userName'));
	}

		public function showPass()
	{
		$userName = DB::table('users')->get();
		return view('usersENG.changePassE', compact('userName'));
	}

		public function showProject()
	{
		$userProject = DB::table('projects')->get();
		return view('usersENG.accountProjectE', compact('userProject'));
	}

		public function showAddproject()
	{
		
		return view('usersENG.addProjectE');
	}


		public function showlove()
	{
		
		return view('usersENG.loveProjectE');
	}


		public function showChInfo()
	{
		
		return view('usersENG.changeInfoE');
	}


		public function showChPass()
	{
		
		return view('usersENG.changePassE');
	}



//dodawanie projektu
	 public function store(Request $request)
    {
      	$input = new file;

        $this->validate($request, Project::$create_validation_rules);
        $input = $request->only('nameProject', 'description', 'subCategory');
        $log = Auth::user()->login;

        // $files = Input::file('images');
        // $destinationPath = 'uploads';
        // $filename = $files->getClientOriginalName();
        // $upload_success = $files ->move($destinationPath, $filename);



        	$file=Input::file('image');
        	$file->move(public_path().'/upload/uploadPhoto', $file->getClientOriginalName());
        	$input['fotoProject'] = $file->getClientOriginalName();

        	$file=Input::file('file');
        	$file->move(public_path().'/upload/uploadFile', $file->getClientOriginalName());
        	$input['fileProject'] = $file->getClientOriginalName();
 
        $input['author']= $log;
      

        project::create($input);
        return redirect('accountProjectE');
     
    }


}



