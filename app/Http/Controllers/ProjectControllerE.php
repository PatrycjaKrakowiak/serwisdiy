<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

//use App\Http\Controllers\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use App\User;
use App\Comments;
use App\project;
use App\Grade;
use File;

class ProjectControllerE extends Controller
{
      	public function index($id)
	{
		$projectName = DB::table('projects')->get();
		$nameCategory = $id;
		return view('/projectsENG/projectCategoryE', compact('projectName', 'nameCategory'));
	}
		public function about($id)
	{
		$projectName = DB::table('projects')->get();
		$idProject = $id;
		$favourite = DB::table('favourite')->get();

		$comments = DB::select('select * from comments where idP = :id', ['id' => $idProject]);
		

		return view('/projectsENG/aboutProjectE', compact('projectName', 'idProject', 'favourite'), array('comments' => $comments));
	}

	public function addF($id)
	{
		$idU = Auth::user()->id;
		$projectName = DB::table('projects')->get();
		$idProject = $id;
		$favourite = DB::table('favourite')->get();

		DB::table('favourite')->insert
		(
   		 ['idU' => $idU, 'idP' => $idProject]
			);
		return Redirect::back();

	}


	public function deleteF($id)
	{	 
		$log = Auth::user()->id;
		$projectName = DB::table('projects')->get();
		$idProject = $id;
		$favourite = DB::table('favourite')->get();

		$del = DB::select('select * from favourite where idU = :id AND idP=:idp', ['id' => $log, 'idp'=>$idProject]);
		foreach ($del as $del2)
		{
		
		 $del3 = $del2->idF;
		 DB::delete('delete from favourite where idF=?',[$del3]);
	    }
		
		return Redirect::back();
	}

	public function addComment($id)
	{
		$projectName = DB::table('projects')->get();
		$idProject = $id;
		$favourite = DB::table('favourite')->get();
	    $log = Auth::user()->id;

		 $input = Input::all();
                $validation = Validator::make($input, Comments::$storevalid);

        if ($validation->passes())
        {               
             $com = new Comments();
		     $com->comment = Input::get('comment');

		     $com->idP=$idProject;
		     $com->idU=$log;
		  
		     $com->save();
   
    		 $validation = array('Correct!');
   
		     return Redirect::back()
		      ->withErrors($validation);            
		  }
		  return Redirect::back()
		            ->withInput()
		            ->withErrors($validation);
       }

       public function addGrade($id)
	{
		$projectName = DB::table('projects')->get();
		$idProject = $id;
		$grade = DB::table('grade')->get();
	    $log = Auth::user()->id;

		 $input = Input::all();

                      
             $grade = new Grade();
		     $grade->valueGrade = Input::get('grade');

		     $grade->idP=$idProject;
		     $grade->idU=$log;

		  
		     $grade->save();
   
    		 $validation = array('Correct!');
   
		     return Redirect::back()
		     ->withErrors($validation);            
		  }
		


}
