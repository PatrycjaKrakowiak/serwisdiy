<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use App\User;
use App\project;
use File;

use Illuminate\Http\Request;

class AdminController extends Controller
{
      public function showAdmin()
    {
        return view('admin.admin');
    }

        public function showAllP()
    {
    	$userProject = DB::table('projects')->get();
        return view('admin.allProjects',compact('userProject'));
    }

        public function showAllU()
    {
    	$userName = DB::table('users')->get();
        return view('admin.allUsers', compact('userName'));
    }

        public function showStats()
    {
        return view('admin.stats');
    }
}
