<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
class AuthControllerE extends Controller
{
    public function login()
    {
    	return view('authENG.loginE');
    }

      public function handleLogin(Request $request)
    {
        $this->validate($request, User::$login_validation_rules);
    	$data = $request->only('login', 'password');
    	if(\Auth::attempt($data))
    	{

    		return \redirect()->intended('homeE');
    	}

    	return back()->withInput()->withErrors(['loginE'=> 'Uncorrect login and/or password']);

    }

    public function logout()
    {
    	\Auth::logout();
    	return redirect()->route('loginE');
    }
}
