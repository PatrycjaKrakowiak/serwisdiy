<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use App\User;
use App\project;
use File;

use Illuminate\Http\Request;

class DataController extends Controller
{
   	public function showName()
	{
		$userName = DB::table('users')->get();
		return view('users.account', compact('userName'));
	}

		public function showInfo()
	{
		$userName = DB::table('users')->get();
		return view('users.changeInfo', compact('userName'));
	}

		public function showPass()
	{
		$userName = DB::table('users')->get();
		return view('users.changePass', compact('userName'));
	}

		public function showProject()
	{
		$userProject = DB::table('projects')->get();
		return view('users.accountProject', compact('userProject'));
	}

		public function showAddproject()
	{
		
		return view('users.addProject');
	}


		public function showlove()
	{
		$projectName = DB::table('projects')->get();
		$idUser = Auth::user()->id;
		$favourite = DB::table('favourite')->get();

		$loveP = DB::select('select * from favourite where idP = :id', ['id' => $idUser]);
		

		return view('users.loveProject', compact('projectName', 'idUser', 'favourite'), array('loveP' => $loveP));
		
	}


		public function showChInfo()
	{
		
		return view('users.changeInfo');
	}


		public function showChPass()
	{
		
		return view('users.changePass');
	}



//dodawanie projektu
	 public function store(Request $request)
    {
      	$input = new file;

        $this->validate($request, Project::$create_validation_rules);
        $input = $request->only('nameProject', 'description', 'subCategory');
        $log = Auth::user()->login;

        // $files = Input::file('images');
        // $destinationPath = 'uploads';
        // $filename = $files->getClientOriginalName();
        // $upload_success = $files ->move($destinationPath, $filename);



        	$file=Input::file('image');
        	$file->move(public_path().'/upload/uploadPhoto', $file->getClientOriginalName());
        	$input['fotoProject'] = $file->getClientOriginalName();

        	$file=Input::file('file');
        	$file->move(public_path().'/upload/uploadFile', $file->getClientOriginalName());
        	$input['fileProject'] = $file->getClientOriginalName();
 
        $input['author']= $log;
      

        project::create($input);
        return redirect('accountProject');
     
    }


}



