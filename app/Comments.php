<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    public $table = 'comments';
     public $timestamps = false;

 protected $fillable = ['idC', 'idU', 'idP','comment'];

 public static $storevalid = array( 
   'comment' => 'required',     
   );
}
