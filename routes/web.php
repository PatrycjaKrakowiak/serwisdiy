<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['web']], function () {
    Route::get('/login', ['as' =>'login', 'uses' =>'AuthController@login']);
    Route::post('/handleLogin', ['as' =>'handleLogin', 'uses' =>'AuthController@handleLogin']);

    Route::get('/home', ['middleware'=>'auth', 'as' =>'home', 'uses' =>'UsersController@home']);
    
     Route::get('/logout','AuthController@logout')->name('logout');
	//Route::get('/logout', ['as' =>'logout', 'uses' =>'AuthController@logout']);
	Route::resource('users', 'UsersController', ['only' => ['create', 'edit','update', 'store']]);

   

    Route::get('/account', ['middleware'=>'auth', 'as' =>'account', 'uses' =>'UsersController@account']);
    Route::get('/account', 'DataController@showName');

    Route::get('/accountProject', 'DataController@showproject');
//Wyswietlanie strony z formularzem dodawania
    Route::get('/addProject', 'DataController@showAddproject');
    Route::post('/addProject', 'DataController@store');

    Route::get('/loveProject', 'DataController@showlove');

    Route::get('/changeInfo', 'DataController@showChInfo');
    Route::get('/changeInfo', 'DataController@showInfo');
   


    Route::get('/changePass', 'DataController@showChPass');
    Route::get('/changePass', 'DataController@showPass');


//wyświetlanie projektów w podkategoriach
    Route::get('/projects/projectCategory/{id}', 'ProjectController@index');

// wyświetlanie informacji o poszczególnym projekcie
    Route::get('/projects/aboutProject/{id}', 'ProjectController@about');

// realizacja stopki:
    Route::get('/contact', 'FooterController@index');


//Panel administratora:
     Route::get('/admin', 'AdminController@showAdmin');
     Route::get('/allProjects', 'AdminController@showAllP');
     Route::get('/allUsers', 'AdminController@showAllU');
     Route::get('/stats', 'AdminController@showStats');

//Usuwanie z ulubionych:
     Route::get('/projects/aboutProject/{id}/deleteF', 'ProjectController@deleteF');

//Dodawanie do ulubionych:
     Route::get('/projects/aboutProject/{id}/addF', 'ProjectController@addF');

//Dodawanie komentarzy:
     Route::post('/projects/aboutProject/{id}', 'ProjectController@addComment');

//Dodawanie oceny:
 //    Route::post('/projects/aboutProject/{id}', 'ProjectController@addGrade');

//Strona kontakt:
     Route::get('/contact', 'PageController@showContact');

//Wyślij wiadomość:
     Route::post('/contact', 'PageController@sendMessage');


     //English version


    Route::get('/loginE', ['as' =>'login', 'uses' =>'AuthControllerE@login']);
    Route::post('/handleLogin', ['as' =>'handleLogin', 'uses' =>'AuthControllerE@handleLogin']);

    Route::get('/homeE', ['middleware'=>'auth', 'as' =>'home', 'uses' =>'UsersControllerE@home']);
    
     Route::get('/logoutE','AuthControllerE@logout')->name('logout');
    //Route::get('/logout', ['as' =>'logout', 'uses' =>'AuthController@logout']);
    Route::resource('users', 'UsersControllerE', ['only' => ['create', 'edit','update', 'store']]);

   

    Route::get('/accountE', ['middleware'=>'auth', 'as' =>'account', 'uses' =>'UsersControllerE@account']);
    Route::get('/accountE', 'DataControllerE@showName');

    Route::get('/accountProjectE', 'DataControllerE@showproject');
//Wyswietlanie strony z formularzem dodawania
    Route::get('/addProjectE', 'DataControllerE@showAddproject');
    Route::post('/addProjectE', 'DataControlleEr@store');

    Route::get('/loveProjectE', 'DataControllerE@showlove');

    Route::get('/changeInfoE', 'DataControllerE@showChInfo');
    Route::get('/changeInfoE', 'DataControllerE@showInfo');
   


    Route::get('/changePassE', 'DataControllerE@showChPass');
    Route::get('/changePassE', 'DataControllerE@showPass');


//wyświetlanie projektów w podkategoriach
    Route::get('/projectsENG/projectCategory/{id}', 'ProjectControllerE@index');

// wyświetlanie informacji o poszczególnym projekcie
    Route::get('/aboutProjectE/{id}', 'ProjectControllerE@about');

// realizacja stopki:
    Route::get('/contactE', 'FooterControllerE@index');


//Panel administratora:
     Route::get('/adminE', 'AdminControllerE@showAdmin');
     Route::get('/allProjectsE', 'AdminControllerE@showAllP');
     Route::get('/allUsersE', 'AdminControllerE@showAllU');
     Route::get('/statsE', 'AdminControllerE@showStats');

//Usuwanie z ulubionych:
     Route::get('/projectsE/aboutProjectE/{id}/deleteF', 'ProjectControllerE@deleteF');

//Dodawanie do ulubionych:
     Route::get('/aboutProjectE/{id}/addF', 'ProjectControllerE@addF');

//Dodawanie komentarzy:
     Route::post('/aboutProjectE/{id}', 'ProjectControllerE@addComment');

//Dodawanie oceny:
     Route::post('/aboutProjectE/{id}', 'ProjectControllerE@addGrade');

//Strona kontakt:
     Route::get('/contactE', 'PageControllerE@showContact');

//Wyślij wiadomość:
     Route::post('/contactE', 'PageControllerE@sendMessage');


});
